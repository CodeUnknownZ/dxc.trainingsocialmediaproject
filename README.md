# Abstract Project Model

## Technology Stack / Software Used

### Frontend

-   Visual Studio Code: 1.71.0
-   Angular: 14.2.1
    -   Typescript: 4.7.4
    -   SCSS
    -   Additional Dependencies:
        -   safe-pipe for preview of hyperlink media
-   NodeJS: 14.17.5
-   NPM: 6.14.14

### Backend

-   Java 8
-   Spring Tool Suite: 4.15.3.RELEASE
-   SpringBoot Framework
-   Additional Dependencies added
    -   [lombok](https://mvnrepository.com/artifact/org.projectlombok/lombok) v1.18.12
    -   [springfox-swagger2](https://mvnrepository.com/artifact/io.springfox/springfox-swagger2) v3.0.0

### Database

-   MySQL8
    -   Tables
        -   User
        -   Role
        -   Post
        -   Media

## Project Flow

![FlowChart](/SocialMediaFlow.drawio.png)
&nbsp;
Or go to [draw.io](https://app.diagrams.net/) and open using [file](/SocialMediaFlow.drawio)

## Project Model
[Entities](SocialMediaBackend/src/main/java/com/dxc/entity/)
-   [User](SocialMediaBackend/src/main/java/com/dxc/entity/User.java)
-   [Role](SocialMediaBackend/src/main/java/com/dxc/entity/Role.java)
-   [Post](SocialMediaBackend/src/main/java/com/dxc/entity/Post.java)
-   [Media](SocialMediaBackend/src/main/java/com/dxc/entity/Media.java)

## Sample Data

Please refer to [Swagger api-docs](http://localhost:8080/v2/api-docs) when Backend code is running,
or refer to [Manually updated](/SampleData.json) (May not be up to date!)
[chrome extension](https://chrome.google.com/webstore/detail/swagger-ui/liacakmdhalagfjlfdofigfoiocghoej?hl=en-US) to support viewing web browser.

---

&nbsp;&nbsp;

# Requirements

Social media has become a popular platform for sharing and creation of content. The aim is to create your own platform using Java, Spring and Angular.
Requirements as below.

1. There should be Feeds page to show the post contents
    - For simplicity, all users will see the same posts
    - The page should be paginated showing 10 posts each time
2. Each post should be able to cater for either a hyperlink or media (video) and a caption
    - Clicking on the post will open the link or play the video
3. Users are required to login to view and post
    - There should be a sign-up page for accounts creation
4. Users should be able to create a post that appears on the Feeds
5. When creating a post, users should be able to upload files or provide a link to the media
    - Local filesystem storage be used
6. A post can only be updated by the user who created it
7. There should be an Admin role that can manage the platform
    - Maintain posts (Delete/update any posts)
    - Maintain users (Delete/update)
8. The system should be able to track the number of views for each post
    - Each click on the post is considered 1 view

Technical considerations

-   Entities should be well-designed and include audit fields such as created by and created date
    -   Refer to Spring documentation https://docs.spring.io/spring-data/jpa/docs/1.7.0.DATAJPA-580-SNAPSHOT/reference/html/auditing.html
    -   https://www.techgeeknext.com/spring-boot/spring-boot-audit-log-jpa
-   APIs should be secured as much as possible. Minimally only logged in user can access.
    -   https://www.youtube.com/playlist?list=PLXy8DQl3058N9wAl7aWVXg6btgrSe97jR
-   There should be validations for inputs and messages should be displayed
-   The design for Angular should be in small reusable components to cater for the requirements
    -   Use different components to display the different media types
-   Consider using 3 types of post to cater for hyperlink, image and video
    -   The fields, link and media, are optional depending on the post type
-   Stored medias should be deleted also when deleting a post

Additional Implementation

-   Developing SMTP protocol to deliver the Email when click on forgot password, user will receive the mail notification to reset the password.
-   Encryption and Decryption business logic to be implemented on password creation to avoid security breach.
