package com.dxc.exception;

import java.util.NoSuchElementException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends NoSuchElementException {

	private static final long serialVersionUID = 1L;

	public NotFoundException(String string) {
		super(string);
	}
	
	public NotFoundException(String string, Throwable cause) {
		super(string, cause);
	}

}
