package com.dxc.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.dxc.dto.UserDTO;
import com.dxc.response.PageableResponse;

public interface UserService extends UserDetailsService{
	UserDTO createUser(UserDTO user);
	UserDTO getUserByID(long ID);
	UserDTO getUserByUsername(String username);
	PageableResponse<UserDTO> getAllUserPageable(int pgNo, int pgSize, String sortBy, String sortDirection);
	UserDTO updateUser(UserDTO userDTO);
	void deleteUserByID(long ID);
	List<UserDTO> getAllUsers();
}
