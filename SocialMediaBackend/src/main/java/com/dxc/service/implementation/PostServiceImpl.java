package com.dxc.service.implementation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.dxc.dto.PostDTO;
import com.dxc.entity.Post;
import com.dxc.exception.NotFoundException;
import com.dxc.repository.PostRepository;
import com.dxc.response.PageableResponse;
import com.dxc.service.PostService;
import com.dxc.utils.AppComUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PostServiceImpl implements PostService {

	@Autowired
	PostRepository postRepository;

	@Autowired
	MediaServiceImpl mediaServiceImpl;

	PostDTO map(Post post) {
		log.info("Post Object {}", post);
		PostDTO postDTO = new PostDTO();

		postDTO.setId(post.getId());
		postDTO.setType(post.getType());
		postDTO.setLink(post.getLink());
		postDTO.setCaption(post.getCaption());
		if (post.getMedia() != null) {
			postDTO.setMediaDTO(mediaServiceImpl.map(post.getMedia()));
		}
		
		postDTO.setViews(post.getViews());
		postDTO.setCreated_by(post.getCreated_by());

		return postDTO;
	}

	Post map(PostDTO postDTO) {
		log.info("PostDTO Object {}", postDTO);
		Post post = new Post();

		post.setId(postDTO.getId());
		post.setType(postDTO.getType());
		post.setLink(postDTO.getLink());
		post.setCaption(postDTO.getCaption());
		if (postDTO.getMediaDTO() != null) {
			post.setMedia(mediaServiceImpl.map(postDTO.getMediaDTO()));
		}
		post.setViews(postDTO.getViews());
		post.setCreated_by(postDTO.getCreated_by());
		
		return post;
	}

	@Override
	public PostDTO createPost(PostDTO postDTO) {
		log.info("PostDTO Object {}", postDTO);
		return map(postRepository.save(map(postDTO)));
	}

	@Override
	public PageableResponse<PostDTO> getAppPosts(int pgNo, int pgSize, String sortBy, String sortDirection) {
		// Get sublist of a list of Post based on paging restriction set in pageable
		Page<Post> posts = postRepository.findAll(AppComUtils.getPageable(pgNo, pgSize, sortBy, sortDirection));

		// Initialize PageableResponse with content class of PostDTO
		PageableResponse<PostDTO> postResponse = new PageableResponse<PostDTO>();

		postResponse.setContent(posts.getContent().stream() // Gets List<Post> and Stream to start mapping
				.map(post -> map(post)) // map Post object to PostDTO object
				.collect(Collectors.toList())// Collect as list and set as content of PageableResponse
		);
		postResponse.setPageNo(posts.getNumber());
		postResponse.setPageSize(posts.getSize());
		postResponse.setTotalEmlement(posts.getTotalElements());
		postResponse.setTotalPages(posts.getTotalPages());
		postResponse.setLast(posts.isLast());

		return postResponse;
	}

	@Override
	public PostDTO getPostByID(long ID) {
		return map(postRepository.findById(ID).orElseThrow(() -> postNotFound(ID)));
	}

	@Override
	public PostDTO updatePost(PostDTO postDTO, long ID) {
		Post post = postRepository.findById(ID).orElseThrow(() -> postNotFound(ID));

		post.setType(postDTO.getType());
		post.setLink(postDTO.getLink());
		post.setCaption(postDTO.getCaption());
		post.setMedia(mediaServiceImpl.map(postDTO.getMediaDTO()));
		post.setViews(postDTO.getViews());

		return map(postRepository.save(post));
	}

	@Override
	public void deletePostByID(long ID) {
		postRepository.delete(postRepository.findById(ID).orElseThrow(() -> postNotFound(ID)));
	}

	private NotFoundException postNotFound(Long ID) {
		return new NotFoundException("Unable to find with post of ID: " + ID);
	}

	public List<PostDTO> getAllAppPosts() {
		return postRepository.findAll().stream().map(post -> map(post)).collect(Collectors.toList());
	}

}
