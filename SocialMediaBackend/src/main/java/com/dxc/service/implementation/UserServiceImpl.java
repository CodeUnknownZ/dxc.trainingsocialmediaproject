package com.dxc.service.implementation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dxc.dto.UserDTO;
import com.dxc.entity.User;
import com.dxc.exception.NotFoundException;
import com.dxc.repository.UserRepository;
import com.dxc.response.PageableResponse;
import com.dxc.service.UserService;
import com.dxc.utils.AppComUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleServiceImpl roleServiceImpl;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	public UserDTO map(User user) {
		log.info("USER INFO MAPPING TO DTO {}", user);

		UserDTO userDTO = new UserDTO();

		userDTO.setId(user.getId());
		userDTO.setEmail(user.getEmail());
		userDTO.setUsername(user.getUsername());
		userDTO.setPassword(user.getPassword());
		log.info("GetRoles {}", user.getRoles());

		userDTO.addRoles(user.getRoles());
		log.info("USERDTO INFO AFTER MAPPED {}", userDTO);
		return userDTO;
	}

	public User map(UserDTO userDTO) {
		log.info("USERSTO INFO MAPPING TO USER {}", userDTO);
		User user = new User();

		user.setId(userDTO.getId());
		user.setEmail(userDTO.getEmail());
		user.setUsername(userDTO.getUsername());
		user.setPassword(userDTO.getPassword());
		log.info("GetRoles {}", userDTO.getRoles());
		user.addRoles(userDTO.getRoles());
		log.info("USER INFO AFTER MAPPED {}", user);
		return user;
	}

	@Override
	public UserDTO createUser(UserDTO userDTO) {
		userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
		log.info("UserDTO {}", userDTO);

		return map(userRepository.save(map(userDTO)));
	}

	@Override
	public UserDTO getUserByID(long ID) {
		return map(userRepository.findById(ID).orElseThrow(() -> userNotFound(ID)));
	}

	@Override
	public UserDTO updateUser(UserDTO userDTO) {
		User user = userRepository.findById(userDTO.getId()).orElseThrow(() -> userNotFound(userDTO.getId()));
//		User user = map(userDTO);
		user.setId(userDTO.getId());
		user.setEmail(userDTO.getEmail());
		user.setUsername(userDTO.getUsername());
		user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
		log.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ USER ROLES {}", user.getRoles());
		// TODO SAVE IS THROWING org.hibernate.HibernateException: Found shared
		// references to a collection: com.dxc.entity.User.roles
		user = userRepository.save(user);
		log.info(user == null ? "User object is null" : "user not null");
		return map(user);
	}

	@Override
	public void deleteUserByID(long ID) {
		User user = userRepository.findById(ID).orElseThrow(() -> userNotFound(ID));
		userRepository.delete(user);
	}

	private NotFoundException userNotFound(long ID) {
		return new NotFoundException("Unable to find with user of ID: " + ID);
	}

	private NotFoundException userNotFound(String Username) {
		return new NotFoundException("Unable to find any users with username of: " + Username);
	}

	@Override
	public PageableResponse<UserDTO> getAllUserPageable(int pgNo, int pgSize, String sortBy, String sortDirection) {
		// Get sublist of a list of User based on paging restriction set in pageable
		Page<User> users = userRepository.findAll(AppComUtils.getPageable(pgNo, pgSize, sortBy, sortDirection));

		// Initialize PageableResponse with content class of UserDTO
		PageableResponse<UserDTO> userResponse = new PageableResponse<UserDTO>();

		userResponse.setContent(users.getContent().stream() // Gets List<User> and Stream to start mapping
				.map(user -> map(user)) // map User object to UserDTO object
				.collect(Collectors.toList())); // Collect as list and set as content of PageableResponse
		userResponse.setPageNo(users.getNumber());
		userResponse.setPageSize(users.getSize());
		userResponse.setTotalEmlement(users.getTotalElements());
		userResponse.setTotalPages(users.getTotalPages());
		userResponse.setLast(users.isLast());
		return userResponse;
	}
	
	@Override
	public List<UserDTO> getAllUsers() {
		return userRepository.findAll().stream().map(user -> map(user)).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public User loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepository.findByUsername(username).orElseThrow(() -> userNotFound(username));
	}

	@Override
	public UserDTO getUserByUsername(String username) {
		return map(this.loadUserByUsername(username));
	}

}
