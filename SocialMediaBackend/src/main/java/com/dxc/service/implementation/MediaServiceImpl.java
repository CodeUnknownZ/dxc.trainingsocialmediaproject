package com.dxc.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.dxc.entity.Media;
import com.dxc.exception.FileStorageException;
import com.dxc.exception.NotFoundException;
import com.dxc.dto.MediaDTO;
import com.dxc.dto.PostDTO;
import com.dxc.repository.MediaRepository;
import com.dxc.repository.PostRepository;
import com.dxc.service.MediaService;

@Service
public class MediaServiceImpl implements MediaService {

	@Autowired
	MediaRepository mediaRepository;

	@Autowired
	PostRepository postRepository;

	@Autowired
	PostServiceImpl postServiceImpl;

	MediaDTO map(Media media) {
		MediaDTO mediaDTO = new MediaDTO();

		mediaDTO.setId(media.getId());
		mediaDTO.setFileName(media.getFileName());
		mediaDTO.setFileType(media.getFileType());
		mediaDTO.setContent(media.getContent());
		if (media.getPost() != null) {
			mediaDTO.setPostDTO(postServiceImpl.map(media.getPost()));
		}
		return mediaDTO;
	}

	Media map(MediaDTO mediaDTO) {
		Media media = new Media();

		media.setId(mediaDTO.getId());
		media.setFileName(mediaDTO.getFileName());
		media.setFileType(mediaDTO.getFileType());
		media.setContent(mediaDTO.getContent());
		if (mediaDTO.getPostDTO() != null) {
			media.setPost(postServiceImpl.map(mediaDTO.getPostDTO()));
		}
		return media;
	}

	@Override
	public MediaDTO createMedia(MediaDTO mediaDTO) {
		String fileName = StringUtils.cleanPath(mediaDTO.getFileName());

		try {
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry, FileName contains invalid path sequence: " + fileName);
			}
			Media media = map(mediaDTO);
			return map(mediaRepository.save(media));
		} catch (Exception e) {
			throw new FileStorageException("Could not store file " + fileName + " please try again", e);
		}
	}

	@Override
	public MediaDTO getMeidaByID(String ID) {
		return map(mediaRepository.findById(ID).orElseThrow(() -> fileNotFoundWith(ID)));
	}

	@Override
	public MediaDTO getMediaByPost(PostDTO postDTO) {
		return map(mediaRepository.findByPost(postRepository.findById(postDTO.getId())
				.orElseThrow(() -> new NotFoundException("No file was found with realted post"))));
	}

	@Override
	public MediaDTO updateMedia(MediaDTO mediaDTO, String ID) {
		Media media = mediaRepository.findById(ID).orElseThrow(() -> fileNotFoundWith(ID));

		media.setFileName(mediaDTO.getFileName());
		media.setFileType(mediaDTO.getFileType());
		media.setContent(mediaDTO.getContent());
		media.setPost(postServiceImpl.map(mediaDTO.getPostDTO()));

		return map(mediaRepository.save(media));
	}

	@Override
	public void deleteMediaByID(String ID) {
		Media media = mediaRepository.findById(ID).orElseThrow(() -> fileNotFoundWith(ID));

		mediaRepository.delete(media);
	}

	private NotFoundException fileNotFoundWith(String ID) {
		return new NotFoundException("File Not found with ID" + ID);
	}

}
