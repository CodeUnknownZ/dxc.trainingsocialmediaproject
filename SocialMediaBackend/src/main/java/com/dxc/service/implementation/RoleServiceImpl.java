package com.dxc.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.entity.Role;
import com.dxc.exception.NotFoundException;
import com.dxc.repository.RoleRepository;
import com.dxc.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleRepository roleRepository;

	@Override
	public Role createRole(Role role) {
		return roleRepository.save(role);
	}

	@Override
	public Role getRoleByID(long ID) {
		return roleRepository.findById(ID).orElseThrow(() -> roleNotFound(ID));
	}

	@Override
	public Role updateRole(Role role, long ID) {
		Role role2 = roleRepository.findById(ID).orElseThrow(() -> roleNotFound(ID));
		role2.setName(role.getName());
		return roleRepository.save(role);
	}

	@Override
	public void deleteRoleByID(long ID) {
		Role role = roleRepository.findById(ID).orElseThrow(() -> roleNotFound(ID));
		roleRepository.delete(role);
	}

	private NotFoundException roleNotFound(long ID) {
		return new NotFoundException("Unable to find role with ID: " + ID);
	}

	public List<Role> getAllRoles() {
		return roleRepository.findAll();
	}
}
