package com.dxc.service;

import com.dxc.dto.MediaDTO;
import com.dxc.dto.PostDTO;

public interface MediaService {
	MediaDTO createMedia(MediaDTO media);
	MediaDTO getMeidaByID(String ID);
	MediaDTO getMediaByPost(PostDTO post);
	MediaDTO updateMedia(MediaDTO media,String ID);
	void deleteMediaByID(String ID);
}
