package com.dxc.service;

import com.dxc.entity.Role;

public interface RoleService {
	Role createRole(Role role);

	Role getRoleByID(long ID);

	Role updateRole(Role role, long ID);

	void deleteRoleByID(long ID);

	//	PageableResponse<RoleDTO> getAllRoles(int pgNo, int pgSize, String sortBy, String sortDirection);
}
