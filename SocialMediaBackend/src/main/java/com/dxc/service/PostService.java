package com.dxc.service;

import com.dxc.dto.PostDTO;
import com.dxc.response.PageableResponse;

public interface PostService {
	PostDTO createPost(PostDTO post);
	PageableResponse<PostDTO> getAppPosts(int pgNo, int pgSize, String sortBy, String sortDirection);
	PostDTO getPostByID(long id);
	PostDTO updatePost(PostDTO post, long id);
	void deletePostByID(long id);
}
