package com.dxc.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.dxc.config.SecurityConfig.IsUser;
import com.dxc.dto.MediaDTO;
import com.dxc.response.MediaResponse;
import com.dxc.service.implementation.MediaServiceImpl;
import com.dxc.utils.AppComUtils;

@RestController
@RequestMapping("/api/media")
public class MediaController {

	@Autowired
	private MediaServiceImpl mediaServiceImpl;

	@IsUser
	@GetMapping("/{filename:.+}")
	public ResponseEntity<Resource> downloadMedia(@PathVariable String fileName, HttpServletRequest request) {
		MediaDTO mediaDTO = mediaServiceImpl.getMeidaByID(fileName);

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(mediaDTO.getFileType()))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment:filename=\"" + mediaDTO.getFileName() + "\"")
				.body(new ByteArrayResource(mediaDTO.getContent()));
	}

	@IsUser
	@PostMapping
	public MediaResponse uploadMedia(@RequestParam("file") MultipartFile file) {
		MediaDTO mediaDTO = mediaServiceImpl.createMedia((MediaDTO) file);

		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/media/")
				.path(mediaDTO.getFileName()).toUriString();
		return new MediaResponse(mediaDTO.getFileName(), fileDownloadUri, file.getContentType(), file.getSize());
	}

	@IsUser
	@PutMapping("/{filename:.+}")
	public MediaResponse updateMedia(@RequestParam("file") MultipartFile file, @PathVariable String fileName) {
		MediaDTO mediaDTO = mediaServiceImpl.updateMedia((MediaDTO) file, fileName);

		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/media/")
				.path(mediaDTO.getFileName()).toUriString();
		return new MediaResponse(mediaDTO.getFileName(), fileDownloadUri, file.getContentType(), file.getSize());
	}

	@IsUser
	@DeleteMapping("/{filename:.+}")
	public ResponseEntity<String> deleteMedia(@PathVariable String fileName) {
		mediaServiceImpl.deleteMediaByID(fileName);
		return AppComUtils.ok("File with id: " + fileName + "has been deleted successfully.");
	}
}
