package com.dxc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dxc.config.SecurityConfig.IsUser;
import com.dxc.dto.MediaDTO;
import com.dxc.dto.PostDTO;
import com.dxc.response.PageableResponse;
import com.dxc.service.implementation.MediaServiceImpl;
import com.dxc.service.implementation.PostServiceImpl;
import com.dxc.utils.AppComUtils;
import com.dxc.utils.AppConstants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/post")
public class PostController {

	@Autowired
	PostServiceImpl postServiceImpl;

	@Autowired
	MediaServiceImpl mediaServiceImpl;

	@IsUser
	@GetMapping("/{id}")
	public ResponseEntity<PostDTO> getPost(@PathVariable(name = "id") long ID) {
		return AppComUtils.ok(postServiceImpl.getPostByID(ID));
	}
	@IsUser
	@PostMapping
	public ResponseEntity<PostDTO> createPost(@RequestBody PostDTO postDTO) {
		log.info("PostDTO Object in controller{}",postDTO);
		return AppComUtils.ok(postServiceImpl.createPost(postDTO));
	}
	@IsUser
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deletePost(@PathVariable(name = "id") long ID) {
		postServiceImpl.deletePostByID(ID);
		return AppComUtils.ok("Post with ID : " + ID + "has been deleted successfuly");
	}
	@IsUser
	@PutMapping("/{id}")
	public ResponseEntity<PostDTO> updatePost(@RequestBody PostDTO postDTO, @PathVariable(name = "id") long ID) {
		return AppComUtils.ok(postServiceImpl.updatePost(postDTO, ID));
	}
	@IsUser
	@GetMapping("/{id}/media")
	public ResponseEntity<Resource> getRelatedMedia(@PathVariable(name = "id") long ID) {
		MediaDTO mediaDTO = mediaServiceImpl.getMediaByPost(postServiceImpl.getPostByID(ID));

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(mediaDTO.getFileType()))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment:filename=\"" + mediaDTO.getFileName() + "\"")
				.body(new ByteArrayResource(mediaDTO.getContent()));
	}
	@IsUser
	@GetMapping("/")
	public PageableResponse<PostDTO> getPostForHomePage(
			@RequestParam(value = "pgNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pgNo,
			@RequestParam(value = "pgSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pgSize,
			@RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
			@RequestParam(value = "sortDirection", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDirection) {
		return postServiceImpl.getAppPosts(pgNo, pgSize, sortBy, sortBy);
	}
	
	@IsUser
	@GetMapping("/getAll")
	public ResponseEntity<List<PostDTO>> getPostForHomePage() {
		return AppComUtils.ok(postServiceImpl.getAllAppPosts());
	}
	
	
}
