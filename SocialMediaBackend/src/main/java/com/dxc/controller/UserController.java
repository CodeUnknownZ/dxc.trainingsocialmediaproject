package com.dxc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.dxc.config.SecurityConfig.IsAdmin;
import com.dxc.config.SecurityConfig.IsUser;
import com.dxc.dto.UserDTO;
import com.dxc.response.PageableResponse;
import com.dxc.service.implementation.UserServiceImpl;
import com.dxc.utils.AppComUtils;
import com.dxc.utils.AppConstants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	private UserServiceImpl userServiceImpl;

	@IsAdmin
	@GetMapping("/getAllPageable")
	public PageableResponse<UserDTO> getAllUsers(
			@RequestParam(value = "pgNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pgNo,
			@RequestParam(value = "pgSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pgSize,
			@RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
			@RequestParam(value = "sortDirection", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDirection
			) {
		return userServiceImpl.getAllUserPageable(pgNo, pgSize, sortBy, sortDirection);
	}
	
	@IsAdmin
	@GetMapping("/getAll")
	public ResponseEntity<List<UserDTO>> getAllUsers() {
		return AppComUtils.ok(userServiceImpl.getAllUsers());
	}

	@IsAdmin @IsUser
	@GetMapping("/{id}")
	public ResponseEntity<UserDTO> getUser(@PathVariable(name = "id") long ID) {
		return AppComUtils.ok(userServiceImpl.getUserByID(ID));
	}

	@PostMapping
	public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO) {
		return AppComUtils.ok(userServiceImpl.createUser(userDTO));
	}

	@PostMapping("/update")
	public ResponseEntity<UserDTO> updateUser(@RequestBody UserDTO userDTO) {
		return AppComUtils.ok(userServiceImpl.updateUser(userDTO));
	}

	@IsAdmin @IsUser
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable(name = "id") long ID) {
		userServiceImpl.deleteUserByID(ID);
		return AppComUtils.ok("User with id: " + ID + " has been deleted successfully.");
	}
	
//	@PostMapping("/check")
//	public ResponseEntity<String> isUsernameTaken(@RequestBody String username) {
//		if(userServiceImpl.getUserByUsername(username) != null) {
//			return AppComUtils.ok("Username is already taken");
//		} else return null;
//	}
	
	@PostMapping("/username")
	public ResponseEntity<UserDTO> getUserByUsername(@RequestBody String username) {
		UserDTO userDTO = userServiceImpl.getUserByUsername(username);
		userDTO.setPassword(null); 
		return AppComUtils.ok(userDTO);
	}
	
	
}
