package com.dxc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.dxc.dto.UserDTO;
import com.dxc.response.AuthResponse;
import com.dxc.service.implementation.UserServiceImpl;
import com.dxc.session.InMemorySessionRegistry;
import com.dxc.utils.AppComUtils;
import com.dxc.utils.AppConstants;

@RestController
public class AuthController {

    @Autowired
    public AuthenticationManager manager;

    @Autowired
    public InMemorySessionRegistry sessionRegistry;

    @Autowired
    UserServiceImpl userServiceImpl;

    @PostMapping("/api/login")
    public ResponseEntity<AuthResponse> login(@RequestBody UserDTO userDTO) {

        manager.authenticate(new UsernamePasswordAuthenticationToken(userDTO.getUsername(), userDTO.getPassword()));

        UserDTO userDTO2 = userServiceImpl.getUserByUsername(userDTO.getUsername());

        final String sessionId = sessionRegistry.registerSession(userDTO2.getUsername());

        return AppComUtils.ok(new AuthResponse()
                .setSessionId(sessionId)
                .setId(userDTO2.getId())
                .setUsername(userDTO2.getUsername())
                .setEmail(userDTO2.getEmail())
                .setRoles(userDTO2.getRoles()));
    }

    @PostMapping("/api/logout")
    public ResponseEntity<AuthResponse> logout(@RequestBody UserDTO userDTO,
            @RequestHeader(name = HttpHeaders.AUTHORIZATION) String token) {
        userDTO = userServiceImpl.getUserByUsername(userDTO.getUsername());
        sessionRegistry.removeSession(token);
        SecurityContextHolder.getContext().setAuthentication(null);
        return AppComUtils.ok(new AuthResponse()
                .setSessionId(token)
                .setId(userDTO.getId())
                .setUsername(userDTO.getUsername())
                .setLogout(true));
    }

    @GetMapping("/login")
    public RedirectView login() {
        // redirect user to frontend login screen
        return new RedirectView(AppConstants.FRONTEND_BASE_URL + "/login");
    }

}
