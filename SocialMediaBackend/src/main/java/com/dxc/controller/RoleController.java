package com.dxc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dxc.config.SecurityConfig.IsAdmin;
import com.dxc.config.SecurityConfig.IsUser;
import com.dxc.entity.Role;
import com.dxc.service.implementation.RoleServiceImpl;
import com.dxc.utils.AppComUtils;

@RestController
@RequestMapping("/api/role")
public class RoleController {

	@Autowired
	RoleServiceImpl roleServiceImpl;

	@GetMapping("/getAll")
	public List<Role> getAllRoles() {
		return roleServiceImpl.getAllRoles();
	}

	@IsAdmin
	@IsUser
	@GetMapping("/{id}")
	public ResponseEntity<Role> getRole(@PathVariable(name = "id") long ID) {
		return AppComUtils.ok(roleServiceImpl.getRoleByID(ID));
	}

	@IsAdmin
	@PostMapping
	public ResponseEntity<Role> createRole(@RequestBody Role role) {
		return AppComUtils.ok(roleServiceImpl.createRole(role));
	}

	@IsAdmin
	@PutMapping("/{id}")
	public ResponseEntity<Role> updateRole(@RequestBody Role role, @PathVariable(name = "id") long ID) {
		return AppComUtils.ok(roleServiceImpl.updateRole(role, ID));
	}

	@IsAdmin
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteRole(@PathVariable(name = "id") long ID) {
		roleServiceImpl.deleteRoleByID(ID);
		return AppComUtils.ok("Role with ID: " + ID + " has been deleted successfully.");
	}
}
