package com.dxc.session;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.dxc.entity.User;
import com.dxc.service.implementation.UserServiceImpl;

@Component
public class SessionFilter extends OncePerRequestFilter {
	@Autowired
	private InMemorySessionRegistry sessionRegistry;

	@Autowired
	private UserServiceImpl userServiceImpl;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		final String sessionID = request.getHeader(HttpHeaders.AUTHORIZATION);

		if (sessionID != null) {
			final String username = sessionRegistry.getUsernameForSession(sessionID);
			if (username != null) {
				User user = userServiceImpl.loadUserByUsername(username);

				final UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, null,
						user.getAuthorities());
				auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(auth);
			}

		}
		filterChain.doFilter(request, response);
	}

}
