package com.dxc.session;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.UUID;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class InMemorySessionRegistry {
	public static final HashMap<String, String> SESSIONS = new HashMap<>();

	// Method to register session for said username
	public String registerSession(final String username) {
		if (username == null) {
			throw new RuntimeException("Username needs to provided");
		}

		final String sessionId = genSessionID();
		SESSIONS.put(sessionId, username);
		logActiveSession();
		return sessionId;
	}

	// Get username by session ID
	public String getUsernameForSession(final String sessionId) {
		return SESSIONS.get(sessionId);
	}

	// Random Session ID generator
	private String genSessionID() {
		return new String(Base64.getEncoder().encode(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8)));
	}

	public void removeSession(String sessionId) {
		SESSIONS.remove(sessionId);
		logActiveSession();
	}

	private void logActiveSession() {
		log.info("Active Sessions {}", SESSIONS);
	}
}
