package com.dxc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dxc.entity.*;

@Repository
public interface MediaRepository extends JpaRepository<Media, String>{
	public Media findByPost(Post post);

}
