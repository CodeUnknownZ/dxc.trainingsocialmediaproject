package com.dxc.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dxc.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
//	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Optional<User> findByUsername(String username);

}
