package com.dxc.audit;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.dxc.service.implementation.UserServiceImpl;

public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Override
    public Optional<String> getCurrentAuditor() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String currentPrincipalName = authentication.getName();
            return Optional.ofNullable(userServiceImpl.getUserByUsername(currentPrincipalName).getUsername());
        } catch (Exception e) {
            return Optional.ofNullable("anonymous");
        }
    }

}