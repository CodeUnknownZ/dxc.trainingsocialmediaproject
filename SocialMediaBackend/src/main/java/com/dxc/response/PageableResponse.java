package com.dxc.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageableResponse<C> {
	private List<C> content;
	private int pageNo;
	private int pageSize;
	private long totalEmlement;
	private int totalPages;
	private boolean last;
}
