package com.dxc.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MediaResponse {
	private String fileName;
	private String fileDownload;
	private String fileType;
	private long fileSize;
}
