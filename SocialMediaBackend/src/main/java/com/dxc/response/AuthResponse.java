package com.dxc.response;

import java.util.Set;

import com.dxc.entity.Role;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class AuthResponse {
	@Getter private String sessionId;
	@Getter private long id;
	@Getter private String username;
	@Getter private String email;
	@Getter private Set<Role> roles;
	@Getter private boolean logout;

	public AuthResponse setSessionId(String sessionId) {
		this.sessionId = sessionId;
		return this;
	}

	public AuthResponse setId(long id) {
		this.id = id;
		return this;
	}

	public AuthResponse setUsername(String username) {
		this.username = username;
		return this;
	}
	
	public AuthResponse setEmail(String email) {
		this.email = email;
		return this;
	}

	public AuthResponse setRoles(Set<Role> roles) {
		this.roles = roles;
		return this;
	}
	
	public AuthResponse setLogout(boolean logout) {
		this.logout = logout;
		return this;
	}
}
