package com.dxc.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class AppComUtils {
    public static Pageable getPageable(int pgNo, int pgSize, String sortBy, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        return PageRequest.of(pgNo, pgSize, sort);
    }

    // Method Overload for default pagable
    public static Pageable getPageable() {
        Sort sort = AppConstants.DEFAULT_SORT_DIRECTION.equalsIgnoreCase(Sort.Direction.ASC.name())
                ? Sort.by(AppConstants.DEFAULT_SORT_BY).ascending()
                : Sort.by(AppConstants.DEFAULT_SORT_BY).descending();
        return PageRequest.of(Integer.parseInt(AppConstants.DEFAULT_PAGE_NUMBER),
                Integer.parseInt(AppConstants.DEFAULT_PAGE_SIZE), sort);
    }

    // ResoponseEntity Utils
    public static <C> ResponseEntity<C> ok(C content) {
        return new ResponseEntity<C>(content, HttpStatus.OK);
    }

    public static <C> ResponseEntity<C> expectationFailed(C content) {
        return new ResponseEntity<C>(content, HttpStatus.EXPECTATION_FAILED);
    }
}
