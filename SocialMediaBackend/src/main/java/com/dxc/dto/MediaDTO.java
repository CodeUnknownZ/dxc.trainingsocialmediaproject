package com.dxc.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MediaDTO {
	private String id;
	private String fileName;
	private String fileType;
	
	byte[] content;
	
	PostDTO postDTO;
}
