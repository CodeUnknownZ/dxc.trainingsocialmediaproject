package com.dxc.dto;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.dxc.entity.Role;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
	private long id;
	private String email;
	private String username;
	private String password;
	@Setter(value = AccessLevel.NONE)
	private Set<Role> roles = new HashSet<>();

	public Set<Role> getRoles() {
		return this.roles;
	}

	public void addRoles(Set<Role> arole) {
		roles = new HashSet<>();
		for (Iterator<Role> iterator = arole.iterator(); iterator.hasNext();) {
			Role role = (Role) iterator.next();
			roles.add(role);
		}
	}
}
