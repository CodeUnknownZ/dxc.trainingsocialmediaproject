package com.dxc.dto;

import com.dxc.entity.Media;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostDTO {
	private long id;
	private String type;
	private String link;
	private String caption;
	private MediaDTO mediaDTO;
	private long views; // Number of views
	private String created_by;
}
