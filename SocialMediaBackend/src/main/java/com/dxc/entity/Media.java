package com.dxc.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.dxc.audit.Auditable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Media extends Auditable<String> {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	private String fileName;
	private String fileType;

	@Lob
	byte[] content;

	@OneToOne(targetEntity = Post.class)
	Post post;
}
