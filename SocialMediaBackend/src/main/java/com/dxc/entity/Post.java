package com.dxc.entity;

import com.dxc.audit.Auditable;

import java.util.Collection;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedBy;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Post extends Auditable<String> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@PrimaryKeyJoinColumn
	private long id;
	@Column(nullable = false)
	private String type;
	@Column(nullable = true)
	private String link;
	@Column(nullable = false)
	private String caption;
	@OneToOne(targetEntity = Media.class, cascade = CascadeType.ALL)
	private Media media;
	private long views; // Number of views
	
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "users_posts", joinColumns = @JoinColumn(name = "post_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private User user;
    
    @CreatedBy
    @Column(insertable = false , updatable = false)
    private String created_by;
}
