import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { NgbPaginationModule, NgbAlertModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components';
import { RestService } from './services';

import { LoginComponent } from './components/login/login.component';
import { AuthService } from './services/auth.service';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ErrorComponent } from './components/error/error.component';
import { ResetPasswordComponent } from './components/resetpassword/resetpassword.component';
import { CreateComponent } from './components/post/create/create.component';
import { SafePipeModule } from 'safe-pipe';
import { ViewComponent } from './components/post/view/view.component';
import { ListComponent } from './components/post/list/list.component';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RegisterComponent,
        HomeComponent,
        NavbarComponent,
        ErrorComponent,
        ResetPasswordComponent,
        CreateComponent,
        ViewComponent,
        ListComponent,
        UserManagementComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        NgbPaginationModule,
        NgbAlertModule,
        NgbModule,
        SafePipeModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatButtonModule,
        MatDividerModule,
        MatListModule,
        MatDialogModule
    ],
    providers: [RestService, { provide: HTTP_INTERCEPTORS, useClass: AuthService, multi: true }],
    bootstrap: [AppComponent],
})
export class AppModule { }
