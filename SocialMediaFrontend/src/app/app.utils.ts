import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from "@angular/forms";
import { DEBUG_ENABLED } from "src/app/app.constants";

export class CustomValidators {
    static checkSamePassword(fg: FormGroup, controlName: string, confimControlName: string) {
        return (control: AbstractControl): ValidationErrors => {

            // If one of the fields is not yet touched return null for no error
            if (!fg.get(controlName).value || !fg.get(confimControlName).value) {
                return null;
            }
            // If passwords match return with no error
            if (fg.get(controlName).value === fg.get(confimControlName).value) {
                return null;
            }

            // return mismatch error as passwords do not match
            return {
                mismatch: {
                    message: `Passwords does not match. Please re-enter the passwords.`,
                    result: fg.get(controlName).value === fg.get(confimControlName).value,
                    value: control.value
                }
            };
        }
    }
}

export class FormUtils {
    static setFGValidators(form: FormGroup, data: { controlName: string; validator: ValidatorFn[] }[]) {
        let once = false;
        Object.keys(form.controls).forEach(key => {
            const dataObj = data.find(Obj => Obj['controlName'] === key);
            if (!!dataObj) {
                form.get(key).clearValidators();
                form.get(key).setValidators(dataObj['validator']);
                form.get(key).updateValueAndValidity();
            } else {
                if (DEBUG_ENABLED) {
                    if (!once) {
                        console.groupCollapsed('%cFormUtils.setFGValidators()', 'color: yellow;');
                        once = true;
                    }
                    console.warn(`No default validators set for '${key}'`);
                }
                form.get(key).clearValidators();
                form.get(key).setValidators([]);
                form.get(key).updateValueAndValidity();
            }
        });
        if (DEBUG_ENABLED) {
            console.groupEnd();
        }
        return form;
    }

    static convertToBase64(file: File){
        const reader = new FileReader();
        reader.readAsDataURL(file);
        try {
            return new Promise((resolve, rejects) => {
                (reader.onload = () => {
                    resolve(reader.result.toString().split(',', 2)[1]);
                }),
                    (reader.onerror = () => {
                        rejects(`Unable to parse file to Base64.
                        \nInput is ${JSON.stringify(file, null, ' ')}
                        \nReader.error: ${reader.error}`);
                    });
            });
        } catch (err) {
            alert(`Caught error for convertToBase64 was: ${JSON.stringify(err, null, ' ')}`);
            return null;
        }
    }

    static async convertFile(file: any) {
        return await FormUtils.convertToBase64(file).then(
            resolve => {
                return resolve;
            },
            reject => {
                alert(`convert was rejected`);
                return reject;
            }
        );
    }
}