import { Media } from "./media";

export class Post {
    id: number;
    type: string;
    link: string;
    caption: string;
    mediaDTO: Media;
    views: number;
    created_by: string;
}
