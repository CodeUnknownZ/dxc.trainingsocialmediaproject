import { Role } from "./role";

export class User {
    id: number;
    email: string;
    username: string;
    password: string;
    roles: Role[];
    sessionId: string;
    logout: boolean;

    setUsername(username: string) {
        this.username = username;
        return this;
    }

    setPassword(password: string) {
        this.password = password;
        return this;
    }
    setRole(roles: Role[]) {
        this.roles = roles;
        return this;
    }
    setEmail(email: string) {
        this.email = email;
        return this;
    }
}