export class Media {
    id: number;
    fileName: string;
    fileType: string;
    content: File;
    postID: number;
}