import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components';
import { ErrorComponent } from './components/error/error.component';
import { HomeComponent } from './components/home/home.component';
import { CreateComponent } from './components/post/create/create.component';
import { RegisterComponent } from './components/register/register.component';
import { ResetPasswordComponent } from './components/resetpassword/resetpassword.component';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { AuthenticationGuard } from './guards/authentication.guard';
import { IsAdminGuard } from './guards/is-admin.guard';

const routes: Routes = [
    {
        path: "login",
        component: LoginComponent,
        title: "Login Page"
    }, {
        path: "register",
        component: RegisterComponent,
        title: "Registration Page"
    }, {
        path: "reset-password",
        component: ResetPasswordComponent,
        title: "ResetPassword"
    }, {
        path: "home",
        component: HomeComponent,
        title: "HomePage",
        canActivate: [AuthenticationGuard]
    }, {
        path: "post",
        canActivateChild: [AuthenticationGuard],
        children: [{
            path: "new",
            component: CreateComponent,
            title: "New Post"
        }]
    }, {
        path: "mgmt",
        canActivateChild: [AuthenticationGuard, IsAdminGuard],
        children: [{
            path: "user",
            component: UserManagementComponent,
            title: "User Management"
        }]
    }, {
        path: "error",
        component: ErrorComponent
    }, {
        path: "",
        redirectTo: "/login",
        pathMatch: 'full'
    }, {
        path: "**",
        redirectTo: "error",
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
