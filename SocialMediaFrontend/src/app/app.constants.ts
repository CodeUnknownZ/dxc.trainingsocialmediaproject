import { environment } from "src/environments/environment";

export const DEBUG_ENABLED: boolean = environment.production == false;
export const API_BASE_URL: string = environment.api_base_url;
export const APP_NAME: string = "SocialMediaFrontend";







export const APP_REGEX = {
    normalText: '^[0-9a-zA-Z ]*$',
    email: '^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]+)$',
    hyperlink: `^((http:\\/\\/)|(https:\\/\\/))?([a-zA-Z0-9]+[.])+[a-zA-Z]{2,4}(:\\d+)?(\/[~_.\\-a-zA-Z0-9=&%@:]+)*\\??[~_.\\-a-zA-Z0-9=&%@:]*$`
}


// Should be from Backend to be more dynamic other then first option
export const APP_POST_TYPE = [
    { code: "", description: " Select type of post " , disabled: true},
    { code: "link", description: " Post with hyperlink ", disabled: false },
    { code: "media", description: " Post with media ", disabled: false }
];