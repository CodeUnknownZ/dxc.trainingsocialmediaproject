import { HttpRequest } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { APP_REGEX } from 'src/app/app.constants';
import { CustomValidators, FormUtils } from 'src/app/app.utils';
import { User } from 'src/app/entities';
import { RestService } from 'src/app/services';

@Component({
    selector: 'app-resetpassword',
    templateUrl: './resetpassword.component.html',
    styleUrls: ['./resetpassword.component.scss']
})
export class ResetPasswordComponent implements OnInit {

    resetPasswordForm: FormGroup;
    error: boolean;
    oldUser: User;

    constructor(private router: Router, private route: ActivatedRoute, private connect: RestService) { }

    ngOnInit(): void {
        this.route.queryParams
            .subscribe(params => {
                this.error = !!params['error'];
            });

        this.initializeFormG();
        this.oldUser = new User();
    }

    initializeFormG() {
        this.resetPasswordForm = new FormGroup({
            username: new FormControl('', []),
            password: new FormControl('', []),
            password2: new FormControl('', []),
            email: new FormControl('', [])
        });
        let matchValidator = CustomValidators.checkSamePassword(this.resetPasswordForm, "password", "password2");
        FormUtils.setFGValidators(this.resetPasswordForm, [
            { controlName: "username", validator: [Validators.required, Validators.minLength(4), Validators.maxLength(255)] },
            { controlName: "password", validator: [Validators.required, Validators.minLength(4), Validators.maxLength(255)] },
            { controlName: "password2", validator: [Validators.required, Validators.minLength(4), Validators.maxLength(255), matchValidator] },
            { controlName: "email", validator: [Validators.required, Validators.pattern(APP_REGEX.email), Validators.maxLength(255)] },
        ]);

        this.resetPasswordForm.updateValueAndValidity();
    }

    async checkUsername() {
        if (!this.resetPasswordForm.get('username').value) return;
        const resp = await this.connect.serviceCall(
            new HttpRequest(
                "POST",
                this.connect.getAPIBaseURL() + "/user/username",
                this.resetPasswordForm.get('username').value
            ));
        if (!resp.username) {
            this.resetPasswordForm.controls['username'].setErrors({ notFound: { message: "Username Not Found!" } });
        } else {
            this.oldUser = resp;
        }
    }

    async submit() {
        // this.oldUser.setPassword();
        // this.resetPasswordForm.controls['password'].value
        const newUser = new User();
        newUser.id = this.oldUser.id;
        newUser.username = this.oldUser.username;
        newUser.email = this.oldUser.email;
        newUser.roles = this.oldUser.roles;
        newUser.password = this.resetPasswordForm.get('password').value;
        
        const resp = await this.connect.serviceCall(new HttpRequest("POST", this.connect.getAPIBaseURL() + "/user/update", newUser));
        console.log(resp);
        if (resp.error) {
            this.router.navigateByUrl('/reset-password?error=true');
        } else {
            this.router.navigateByUrl('/login?reset=success');
        }
    }

    disableForm() { }

}
