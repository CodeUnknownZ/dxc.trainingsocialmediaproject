import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    isAdmin: boolean = false;
    constructor(private auth: AuthService) { }


    ngOnInit(): void {
        this.isAdmin = this.auth.isUserAdmin();
    }

}
