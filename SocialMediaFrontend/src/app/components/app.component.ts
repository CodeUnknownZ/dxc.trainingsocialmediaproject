import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { APP_NAME } from 'src/app/app.constants';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = APP_NAME;

    links = [
        { title: 'One', fragment: 'one' },
        { title: 'Two', fragment: 'two' }
    ];

    constructor(public route: ActivatedRoute) { }

    logout() {
        // this.http.post('logout', {}).finally(() => {
        //     this.app.logout();
        //     this.router.navigateByUrl('/login');
        // }).subscribe();
    }
}
