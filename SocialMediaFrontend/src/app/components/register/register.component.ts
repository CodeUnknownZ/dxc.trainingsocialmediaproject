import { HttpRequest } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { APP_REGEX } from 'src/app/app.constants';
import { CustomValidators, FormUtils } from 'src/app/app.utils';
import { Role, User } from 'src/app/entities';
import { AuthService, RestService } from 'src/app/services';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    registrationForm: FormGroup;
    roles: Role[];
    error: boolean;
    signupRedirect: boolean;

    constructor(private connect: RestService, private router: Router, private route: ActivatedRoute, private auth: AuthService) { }

    async ngOnInit(): Promise<void> {
        this.route.queryParams
            .subscribe(params => {
                this.error = !!params['error'];
            });

        this.route.queryParams
            .subscribe(params => {
                this.signupRedirect = params['signup'] == "success";
            });

        this.initializeFormG();

        this.roles = await this.connect.serviceCall(new HttpRequest("GET", this.connect.getAPIBaseURL() + "/role/getAll"));
    }

    initializeFormG() {
        this.registrationForm = new FormGroup({
            username: new FormControl('', []),
            password: new FormControl('', []),
            password2: new FormControl('', []),
            email: new FormControl('', [])
        });
        let matchValidator = CustomValidators.checkSamePassword(this.registrationForm, "password", "password2");
        FormUtils.setFGValidators(this.registrationForm, [
            { controlName: "username", validator: [Validators.required, Validators.minLength(4), Validators.maxLength(255)] },
            { controlName: "password", validator: [Validators.required, Validators.minLength(4), Validators.maxLength(255)] },
            { controlName: "password2", validator: [Validators.required, Validators.minLength(4), Validators.maxLength(255), matchValidator] },
            { controlName: "email", validator: [Validators.required, Validators.pattern(APP_REGEX.email), Validators.maxLength(255)] },
        ]);

        this.registrationForm.updateValueAndValidity();
    }


    submit() {
        const nUser = new User()
            .setPassword(this.registrationForm.get('password').value)
            .setUsername(this.registrationForm.get('username').value)
            .setEmail(this.registrationForm.get('email').value)
            .setRole([this.roles[1]]);

        this.createUser(nUser);
    }


    disableForm() {
        this.registrationForm.updateValueAndValidity();
        return this.registrationForm.invalid;
    }

    async createUser(user: User) {
        const resp = await this.connect.serviceCall(new HttpRequest("POST", this.connect.getAPIBaseURL() + "/user", user));
        console.log(resp);
        if (resp.error) {
            this.router.navigateByUrl('/register?error=true');
        } else {
            if (this.auth.isAuthenticated()) {
                this.router.navigateByUrl('/register?signup=success');
            } else {
                this.router.navigateByUrl('/login?signup=success');
            }
        }
    }

    async checkUsername() {
        if (!this.registrationForm.get('username').value) return;
        const resp = await this.connect.serviceCall(
            new HttpRequest(
                "POST",
                this.connect.getAPIBaseURL() + "/user/username",
                this.registrationForm.get('username').value
            ));
        if (!!resp.username) {
            this.registrationForm.controls['username'].setErrors({ taken: { message: "Username is already taken!" } });
        }
    }
}
