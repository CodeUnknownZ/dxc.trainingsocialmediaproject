import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APP_NAME } from 'src/app/app.constants';
import { AuthService } from 'src/app/services';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
    APP_NAME = APP_NAME;
    username: string;
    isAdmin: boolean;
    isAuth: boolean;

    navBarButtons = [
        { code: 'newPost', text: "Create New Post" },
        { code: 'logOut', text: "Logout" },
        { code: 'logIn', text: "Login" },
        { code: 'register', text: "Register Another User" },
        { code: 'userMgmt', text: "Manage Userbase" },
    ]

    constructor(private router: Router, private auth: AuthService) { }

    ngOnInit(): void {
    }

    buttonAction(button: string) {
        switch (button) {
            case this.navBarButtons[0].code:
                this.router.navigateByUrl("/post/new");
                break;

            case this.navBarButtons[3].code:
                this.router.navigateByUrl("/register");
                break;

            case this.navBarButtons[4].code:
                this.router.navigate(["mgmt/user"]);
                break;

            case this.navBarButtons[1].code:
            case this.navBarButtons[2].code:
            default:
                if (button == 'logOut' || button == "" || button == undefined) this.auth.logout();
                this.router.navigateByUrl("/login");
                break;
        }
    }

    isAuthenticated() {
        this.isAuth = this.auth.isAuthenticated();
        this.username = '';
        if (this.isAuth) {
            this.isAdmin = this.auth.isUserAdmin();
            this.username = this.auth.getUsername() + (this.isAdmin ? " (ADMIN)" : "");
            return true;
        }
        return false;
    }
}
