import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/entities';
import { AuthService } from 'src/app/services';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    error: boolean;
    signupRedirect: boolean;

    constructor(private auth: AuthService,private router: Router, private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.route.queryParams
            .subscribe(params => {
                this.error = !!params['error'];
        });

        this.route.queryParams
        .subscribe(params => {
            this.signupRedirect = params['signup']=="success";
        });

        this.loginForm = new FormGroup({
            username: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required]),
        });
    }

    async login() {
        await this.auth.login(
            new User()
                .setUsername(this.loginForm.get('username').value)
                .setPassword(this.loginForm.get('password').value)
        );

        if (this.auth.isAuthenticated()) {
            this.router.navigateByUrl('/home');
        } else {
            this.loginForm.reset();
            this.router.navigateByUrl('/login?error=true');
        }
    }

}
