import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services';

@Component({
    selector: 'app-error',
    templateUrl: './error.component.html',
    styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {

    constructor(private auth: AuthService) { }

    ngOnInit(): void {
    }

    isAuthenticated() {
        return this.auth.isAuthenticated();
    }

}
