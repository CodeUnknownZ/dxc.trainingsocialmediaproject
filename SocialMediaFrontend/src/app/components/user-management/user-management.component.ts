import { Dir } from '@angular/cdk/bidi';
import { HttpRequest } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Role, User } from 'src/app/entities';
import { RestService } from 'src/app/services';

@Component({
    selector: 'app-user-management',
    templateUrl: './user-management.component.html',
    styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {

    userlist: User[];
    message: String;

    constructor(private connect: RestService, private route: ActivatedRoute, private router: Router) { }

    async ngOnInit(): Promise<void> {
        this.message = "";
        this.route.queryParams
            .subscribe(params => {
                this.message = params['delMessage'];
                if (!!this.message) this.getUserList();
            });

        this.getUserList();
    }
    async getUserList() {
        let req = new HttpRequest("GET", this.connect.getAPIBaseURL() + "/user/getAll");
        this.userlist = await this.connect.serviceCall(req);
    }

    async deleteUser(user: User) {
        let req = new HttpRequest("DELETE", this.connect.getAPIBaseURL() + "/user/" + user.id.toString());
        let resp = await this.connect.serviceCall(req);
        this.router.navigateByUrl('/mgmt/user?delMessage=' + resp.error.text);
    }

    hasAdmin(roles: Role[]) {
        for (let i = 0; i < roles.length; i++) {
            const role = roles[i];
            if (role.name == "ADMIN") return true;
        }
        return false;
    }



}
