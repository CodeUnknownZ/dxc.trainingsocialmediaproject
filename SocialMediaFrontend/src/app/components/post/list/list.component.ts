import { HttpRequest } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Post } from 'src/app/entities/post';
import { RestService } from 'src/app/services';
import { ViewComponent } from '../view/view.component';

@Component({
    selector: 'app-post-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    posts: Post[];

    constructor(private connect: RestService, public dialog: MatDialog) { }

    async ngOnInit(): Promise<void> {
        let req = new HttpRequest("GET", this.connect.getAPIBaseURL() + "/post/getAll");
        this.posts = await this.connect.serviceCall(req);
        console.log(this.posts);

    }

    openDialog(index: number): void {

        const dialogRef = this.dialog.open(ViewComponent, {
            width: 'auto',
            height: 'auto',
            maxHeight: '75%',
            maxWidth: '40%',
            data: this.posts[index]
        });

        dialogRef.afterClosed().subscribe(() => {
            console.log('The dialog was closed');
        });
    }
}

