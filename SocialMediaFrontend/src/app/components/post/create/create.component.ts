import { HttpRequest } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { APP_POST_TYPE, APP_REGEX } from 'src/app/app.constants';
import { FormUtils } from 'src/app/app.utils';
import { Media } from 'src/app/entities/media';
import { Post } from 'src/app/entities/post';
import { RestService } from 'src/app/services';

@Component({
    selector: 'app-post-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

    postTypes = APP_POST_TYPE;
    newPostForm: FormGroup;
    txtAPlaceh: string;
    hlPlaceh: string;

    imageUrl: string;
    fileCache: File;

    error: boolean;

    constructor(private connect: RestService, private router: Router, private route: ActivatedRoute) { }

    tempHyperlinkPreview: string;

    ngOnInit(): void {

        this.route.queryParams
            .subscribe(params => {
                this.error = !!params['error'];
            });

        this.txtAPlaceh = "Please enter a caption for the post!";
        this.hlPlaceh = "Please enter the link to the media!";
        this.newPostForm = new FormGroup({
            post_type: new FormControl(''),
            caption: new FormControl('',),
            content: new FormControl(null,)
        });

        FormUtils.setFGValidators(this.newPostForm, [
            { controlName: "post_type", validator: [Validators.required] },
            { controlName: "caption", validator: [Validators.required, Validators.maxLength(255)] },
        ]);
    }

    postTypeSelected() {
        // No value clear validators
        if (!this.newPostForm.get('post_type').value) {
            this.newPostForm.get('content')?.clearValidators();
        }
        // Link
        if (this.newPostForm.get('post_type').value == APP_POST_TYPE[1].code) {
            console.log(APP_REGEX.hyperlink);

            this.newPostForm.get('content').setValidators([Validators.required, Validators.pattern(APP_REGEX.hyperlink)]);
        }
        // Media
        if (this.newPostForm.get('post_type').value == APP_POST_TYPE[2].code) {
            this.newPostForm.get('content').setValidators([Validators.required]);
        }

        // Clear values everytime post type is changed 
        this.newPostForm.get('content').setValue('');
        this.imageUrl = null;
        // this.newPostForm.get('caption').setValue('');

        // Update form
        this.newPostForm.updateValueAndValidity();
    }

    getHyperlink() {
        return this.tempHyperlinkPreview;
    }

    showPreview() {
        // to prevent embed to constantly reloading on input of from
        this.tempHyperlinkPreview = this.newPostForm.get('content').value;
    }

    async createPost() {
        console.log(this.newPostForm);
        let post = new Post();
        post.type = this.newPostForm.get('post_type').value;
        post.caption = this.newPostForm.get('caption').value;
        post.views = 0;
        if (this.newPostForm.get('post_type').value == APP_POST_TYPE[1].code) {
            post.link = this.newPostForm.get('content').value;
        }

        if (this.newPostForm.get('post_type').value == APP_POST_TYPE[2].code) {
            post.mediaDTO = new Media();
            post.mediaDTO.content = await FormUtils.convertFile(this.fileCache);
            post.mediaDTO.fileName = this.fileCache.name
            post.mediaDTO.fileType = this.fileCache.type
        }

        let resp = await this.connect.serviceCall(new HttpRequest("POST", this.connect.getAPIBaseURL() + "/post", post));
        console.log(resp);
        if (resp.id !== 0 || resp.id !== null) {
            this.router.navigateByUrl('/home');
        } else {
            this.router.navigateByUrl('/post/new?error=true');
        }
    }

    onPhotoSelected(photoSelector: HTMLInputElement) {
        let fileReader = new FileReader();
        this.fileCache = photoSelector.files[0];
        fileReader.readAsDataURL(this.fileCache);
        fileReader.addEventListener(
            "loadend",
            ev => {
                this.imageUrl = fileReader.result!.toString();
            }
        );
    }

}
