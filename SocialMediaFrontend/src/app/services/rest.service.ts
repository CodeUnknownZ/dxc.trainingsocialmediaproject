import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DEBUG_ENABLED } from '../app.constants';
import { HttpRequest } from '@angular/common/http';
import { API_BASE_URL } from '../app.constants';

@Injectable({
    providedIn: 'root'
})
export class RestService {

    constructor(private client: HttpClient) { }

    private requestCompleted(RequestURL: string): void {
        if (DEBUG_ENABLED) console.log("\nRequest to " + RequestURL + " has been completed");
    }

    public serviceCall<T>(Request: HttpRequest<T>) {
        return this.serviceRequest<T>(Request).then(
            success => success,
            failed => failed
        );
    }

    private serviceRequest<T>(Request: HttpRequest<T>) {
        let response;
        switch (Request.method) {
            case 'POST':
                response = this.servicePOST(Request.body, Request.url, this.getOptions(Request));
                break;
            case 'DELETE':
                response = this.serviceDELETE(Request.url, this.getOptions(Request));
                break;
            case 'PUT':
                response = this.servicePUT(Request.body, Request.url, this.getOptions(Request));
                break;
            case 'GET':
            default:
                response = this.serviceGET(Request.url, this.getOptions(Request));
                break;
        }

        return response;
    }

    private getOptions<T>(Request: HttpRequest<T>) {
        return {
            headers: Request.headers,
            params: Request.params,
            body: Request.body
        }
    }

    private servicePOST(RequestBody: any, RequestURL: string, option: any) {
        return new Promise((success, failed) =>
            this.client.post(RequestURL, RequestBody, option).subscribe(
                data => success(data),
                error => failed(error),
                () => this.requestCompleted(RequestURL)
            )
        );
    }

    private serviceGET(RequestURL: string, option: any) {
        return new Promise((success, failed) =>
            this.client.get(RequestURL, option).subscribe(
                data => success(data),
                error => failed(error),
                () => this.requestCompleted(RequestURL)
            )
        );
    }

    private servicePUT(RequestBody: any, RequestURL: string, option: any) {
        return new Promise((success, failed) =>
            this.client.put(RequestURL, RequestBody, option).subscribe(
                data => success(data),
                error => failed(error),
                () => this.requestCompleted(RequestURL)
            )
        );
    }

    private serviceDELETE(RequestURL: string, option: any) {
        return new Promise((success, failed) =>
            this.client.delete(RequestURL, option).subscribe(
                data => success(data),
                error => failed(error),
                () => this.requestCompleted(RequestURL)
            )
        );
    }


    /** Return the base url for BE Services
     * @return {*}  {string}
     * @memberof AuthService
     */
    getAPIBaseURL(): string {
        return (API_BASE_URL + "/api");
    }
}
