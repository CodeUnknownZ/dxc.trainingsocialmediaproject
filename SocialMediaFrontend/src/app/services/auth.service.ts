import { Injectable } from '@angular/core';

import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpClient,
    HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs'
import { Role, User } from 'src/app/entities';
import { RestService } from './rest.service';


@Injectable({
    providedIn: 'root'
})
export class AuthService implements HttpInterceptor {

    private user: User = null;
    all_roles: Role[] = [{ "id": 1, "name": "ADMIN" }, { "id": 2, "name": "USER" }];

    constructor(private connect: RestService) { }

    /** Interceptor to inject CreatedBy into headers for every API call
     * @param {HttpRequest<any>} req
     * @param {HttpHandler} next
     * @return {*}  {Observable<HttpEvent<any>>}
     * @memberof AuthService
     */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req.clone({
            setHeaders: {
                CreatedBy: this.isAuthenticated() ? this.user.username : "anonymous",
                // Authorization: this.isAuthenticated() ? ('Basic ' + btoa(this.user.username + ':' + this.user.password)) : ""
                Authorization: this.isAuthenticated() ? this.user.sessionId : ""
            }
        }));
    }

    async login(user: User = this.user) {
        let request = new HttpRequest(
            'POST',
            this.connect.getAPIBaseURL() + "/login",
            user);
        const resp = await this.connect.serviceCall(request);
        if (resp.error || resp.sessionId == undefined) {
            return null;
        } else {
            this.user = resp;
            this.storeUserToSession();
            return resp;
        }
    }

    isAuthenticated(): boolean {
        this.getUserFromSession();
        if (this.user == null) return false;
        if (this.user.logout) this.clearUserFromSession()
        return !(
            this.user.logout ||
            this.user.id == undefined || this.user.id == 0 ||
            this.user.sessionId == undefined || this.user.sessionId.length == 0
        );
    }

    async logout() {
        this.getUserFromSession();
        let request = new HttpRequest(
            'POST',
            this.connect.getAPIBaseURL() + "/logout/",
            this.user
        );
        this.user = await this.connect.serviceCall(request);
        this.clearUserFromSession();
    }

    storeUserToSession(user = this.user) {
        sessionStorage.setItem('Active User', JSON.stringify(user));
        return this;
    }

    clearUserFromSession() {
        this.user = null;
        sessionStorage.removeItem('Active User');
        return this;
    }

    getUserFromSession() {
        this.user = JSON.parse(sessionStorage.getItem('Active User'));
        return this.user;
    }

    isUserAdmin() {
        this.getUserFromSession();
        return !!this.user.roles.find(role => role.name === "ADMIN");
    }

    async getRoles() {
        this.all_roles = await this.connect.serviceCall(new HttpRequest("GET", this.connect.getAPIBaseURL() + "/role/getAll"));
    }

    getUsername(){
        this.getUserFromSession();
        return this.user?.username;
    }

}
